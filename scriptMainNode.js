//Load Environment Variables
require('dotenv').config()
//IPFS
const IpfsClient = require('ipfs-http-client');
const OrbitDB = require('orbit-db');
//Instance of IPFS locally in IPFS daemon
const node = IpfsClient('http://localhost:5001');
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://" + process.env.USERDB + ":" + encodeURIComponent(process.env.PASSDB) + "@localhost:27017/" + process.env.DBNAME;

//Module for get prices
var getEth = require('./getEth');

//ABI from contract DECA ERC20
const abi = require("./ABI.js");

//Web3 and IPC Provider
const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.WebsocketProvider(process.env.INFURAKEY));

//DECA Contract instance
const decaContract = new web3.eth.Contract(abi, process.env.CONTRACTADDRS);

//Function to save data from web3 and contract (ret TotalSupply, Ethereum, Date)
const getGethElems = async (dates) => {
   let ret = {};
   var balanceEthereum = await web3.eth.getBalance(process.env.CONTRACTADDRS);
   var totalSupply = await decaContract.methods.totalSupply().call();
   totalSupply = web3.utils.fromWei(totalSupply, 'ether');
   balanceEthereum = web3.utils.fromWei(balanceEthereum, 'ether');
   ret.DTS = totalSupply;
   ret.ETS = balanceEthereum;
   ret.dates = dates;
   return ret;
}

//Function to save data from orbitDB CC (ret ccTotal, ccTS, dates)
const getEthSum = async (elemsCC, dates) => {
   const ret = {};
   let sum = 0;
   let sumCC = 0;
   for (let key in elemsCC) {
      sum += elemsCC[key]['conversionPrice']['ETH'];
      sumCC += 1;
   }
   ret.ccTotal = sumCC;
   ret.ccTS = sum;
   ret.dates = dates;
   return ret;
}

async function index() {
   //Open and load orbitdb databases
   const orbitdb = await OrbitDB.createInstance(node)
   const db = await orbitdb.docs('/orbitdb/zdpuAykPJ4qtBg2toS2vxr5eaPfGEBJmvGerM7V7x8qn5c8hW/decaCCDB', {
      indexBy: 'CCID'
   });
   await db.load();
   const db2 = await orbitdb.docs('/orbitdb/zdpuB1E3gfmmo17cFD2LEgxsjLzU9pxyW1q1Wde5TNL5QjiJG/decaPrice', {
      indexBy: 'dates'
   });
   await db2.load();
   const db3 = await orbitdb.docs('/orbitdb/zdpuB3ND1iK3NvYQx939ErZ2WXCaDoz4Lw3hazFUkdVupstBv/decaGeth', {
      indexBy: 'dates'
   });
   await db3.load();
   const db4 = await orbitdb.docs('/orbitdb/zdpuB2ZiX2QX8Qwwc1EXxo1y944tG6JcBk5UTLHm9b9Nv3Gna/decaCCTS', {
      indexBy: 'dates'
   });
   await db4.load();
   const dbm = await MongoClient.connect(url, {
         useUnifiedTopology: true
      })
      .catch(err => {
         console.log(err);
      });
   //Flag to save the following data
   let dates = 1595524800;
   while (true) {
      //Compare flag and unixtime to add data
      if (Math.round(new Date().getTime() / 1000) == dates) {
         dates += 300;
         //Get data from functions
         var dbo = dbm.db(process.env.DBNAME);
         try {
            let elemsDECAPrice = await getEth(dates);
            const hash1 = await db2.put(elemsDECAPrice);
            dbo.collection("decaPrice").insertOne(elemsDECAPrice, function (err, res) {
               if (err) throw err;
               console.log("Insert decaPrice")
            });
            console.log(hash1);
         } catch (error) {
            console.log(error);
            console.error("Error in DECA Prices");
         }
         try {
            let elemsGeth = await getGethElems(dates);
            console.log(elemsGeth)
            const hash2 = await db3.put(elemsGeth);
            dbo.collection("decaGeth").insertOne(elemsGeth, function (err, res) {
               if (err) throw err;
               console.log("Insert decaGeth")
            });
            console.log(hash2);
         } catch (error) {
            console.log(error);
            console.error("Error in GETH");
         }
         try {
            const data = await db.get('');
            let ccTotalEth = await getEthSum(data, dates);
            const hash3 = await db4.put(ccTotalEth);
            dbo.collection("decaCCTS").insertOne(ccTotalEth, function (err, res) {
               if (err) throw err;
               console.log("Insert decaCCTS")
            });
            console.log(hash3);
         } catch (error) {
            console.log(error);
            console.error("Error in CCDB")
         }
      }
   }
}

index();