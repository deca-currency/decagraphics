# DecaGraphics

DECA Charts Code and settings to calculate price, quantity and percentage of reserve in carbon credits.

**IMPORTANT: Please verify that the database address matches the smart contract address.** 

Official Carbon credits database address ([orbitdb](https://github.com/orbitdb/orbit-db))
**/orbitdb/zdpuAykPJ4qtBg2toS2vxr5eaPfGEBJmvGerM7V7x8qn5c8hW/decaCCDB**

## Table of Contents

[[_TOC_]] 

## General Folder Structure

```sh
├── ABI.js
├── getEth.js
├── index.js
├── package.json
├── README.md
├── scriptChildNode.js
├── scriptMainNode.js
└── services
    ├── decaGraphicsChild.service
    └── decaGraphicsMain.service
```

## Prerequisites

Before you begin, ensure you have met the following requirements:

* Node.js >= 10

## Installation

**Download and install Node.js v10.x and npm.**

> Using Ubuntu

```sh
   $ curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
   $ sudo apt-get install -y nodejs
```

> Using Debian, as root

```sh
   $ curl -sL https://deb.nodesource.com/setup_10.x | bash - 
   $ apt-get install -y nodejs
```

**Clone the repo, switch to decagraphics and run npm install**

```sh
   $ git clone https://gitlab.com/deca-currency/decagraphics.git
   $ cd decagraphics
   $ npm install
```

> It's recommended to fix security vulnerabilities in dependencies using the command.

```sh
   $ npm audit fix
```

It is necessary to have installed mongodb locally in case of making a connection to another server modify the connection url of mongodb.

> Once the installation is done, it is necessary to create a database with three collections with the following names:
```sh
   $ mongo
   use DecaGraphics
   db.createCollection("decaPrice")
   db.createCollection("decaGeth")
   db.createCollection("decaCCTS")
```

> After that it is necessary to set the environment variables in an .env file as in the example .envSample file.

```sh
   APIKEY=
   USERDB=
   PASSDB=
   DBNAME=
   INFURAKEY=
   CONTRACTADDRS=
```

In both scripts it is necessary to set the variable 'dates' to the following date in unixtime from there the script will start to run the calculations automatically.

**Once everything is configured it is necessary to have ipfs running as a service the configuration can be found in [carboncreditsbacklog](https://gitlab.com/deca-currency/carboncreditsbacklog) as well connecting to the nodes mentioned there in order to replicate the orbitdb carbon credits database bd.**

> The scripts can be tested using node as well the service rest which will take the data from mongodb and expose it in a json format in order to do the calculations on the web page

```sh
   $ node scriptMainNode.js   
   $ node index.js
```

To verify the data can be done from a browser taking into account the use of cors or from a bash.

```sh
   $ curl localhost:5000/decaPrice
   $ curl localhost:5000/decaGeth
   $ curl localhost:5000/decaCCTS 
```

## Development

* The dependencies are already in the package.json 
* Ethereum Node 
* Bitcoinaverage Api

The rest service was made in express.js and cors was applied to accept only requests coming from deca.eco as well as helmet.js was used to give a better security to the application and the good practices of [express](https://expressjs.com/es/advanced/best-practice-security.html).

The main script is scriptMainNode.js performs the calculations found in the document [DECAAnalyticsETHPrice](./pdf/DECAAnalyticsETHPrice.pdf) the other script is backup and is used on another node in case the main one fails.


## Deploy

### Prerequisites

* Nginx >= 1.14.0
* Certbot >= 1.6

Install Nginx 

```sh
   $ sudo apt update
   $ sudo apt install nginx
``` 

Install Certbot

```sh
   $ sudo add-apt-repository ppa:certbot/certbot
```

> The script is configured as a service to be able to perform the calculations automatically. The configuration file is located in the service folder.

```sh
   $ sudo vim /etc/systemd/system/decaGraphics.service 
   [Unit]
    Description=Deca Graphics Updater

    [Service]
    WorkingDirectory=/home/nodemaster/decagraphics/
    ExecStart=/usr/local/bin/node /home/nodemaster/decagraphics/scriptMainNode.js
    User=nodemaster
    Restart=always
    LimitNOFILE=10240

    [Install]
    WantedBy=multi-user.target
```

**NOTE: in this example user that runs ipfs, node and orbitdb instance is nodemaster, also the node location is at /usr/local/bin/node**

**NOTE1: set WorkingDirectory where is the repository, for this example /home/nodemaster/decagraphics/**

**NOTE2: set where is index.js mostly in the repository as follow path to node path, for this example /home/nodemaster/decagraphics/scriptMainNode.js**

> Enable the service

```sh
 $ sudo systemctl daemon-reload
 $ sudo systemctl enable decaGraphics.service 
 $ sudo systemctl start decaGraphics.service 
 $ sudo systemctl status decaGraphics.service 
```
**NOTE: service must be set as active (running), if not please verify the preview steps**

> It is possible to check in the service logs if any error occurred in the service.

```sh
● graphicsDeca.service - Graphing Calculator
   Loaded: loaded (/lib/systemd/system/graphicsDeca.service; enabled; vendor preset: enabled)
   Active: active (running) since Thu 2020-07-23 17:18:50 UTC; 4h 23min ago
 Main PID: 629182 (node)
    Tasks: 12 (limit: 464185)
   CGroup: /system.slice/graphicsDeca.service
           └─629182 /usr/bin/node /home/nodemaster/decagraphics/scriptMainNode.js

Jul 23 21:40:02 icowebsrv node[629182]: zdpuAzCgWw1vAt8yKXFUR49uyhuD4W2jN4fKVmzwXwc9QvKzm
Jul 23 21:40:02 icowebsrv node[629182]: Insert decaPrice
Jul 23 21:40:02 icowebsrv node[629182]: zdpuB1YDWAcW9Ljq6WFo4HKXoaHRYKXfL1B9H4dMDRttcGzXF
Jul 23 21:40:02 icowebsrv node[629182]: Insert decaGeth
Jul 23 21:40:02 icowebsrv node[629182]: zdpuAwMn2mjAMAq9w2siJzi6wqc1W7rDi1fw61iuJdaUTnji8
Jul 23 21:40:02 icowebsrv node[629182]: Insert decaCCTS
```

Once the service is working properly it is necessary to configure the service subtract to be able to access the data from [deca.eco](https://deca.eco/).

In the nginx config add the proxy pass



```sh
$ vim /etc/nginx/conf.d/example.com.conf
server {
    listen         80 default_server;
    listen         [::]:80 default_server;
    server_name    example.com www.example.com;
    root           /var/www/example.com;
    index          index.html;
    try_files $uri /index.html;
    location /  {
        proxy_pass http://localhost:5000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
```

**Remember to open the port in the firewall**

**Remember to leave the rest service running in a tmux session and check the port.**

**Now restart up Nginx!**


### Add SSL Certificate

```sh
   $ sudo certbot --nginx -d api.deca.eco 
```

**It is recommended to redirect all http traffic to https**

Finally renew the certificate with the following command and restart nginx

```sh
$ sudo certbot renew --dry-run
$ sudo service nginx restart
```

# Information and contacts.

***Developers***
- Jose [jose@deca.eco](mailto:jose@deca.eco)