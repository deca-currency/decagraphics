require('dotenv').config()
var helmet = require('helmet');
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://" + process.env.USERDB + ":" + encodeURIComponent(process.env.PASSDB) + "@localhost:27017/" + process.env.DBNAME
const express = require("express");
const app = express();
const cors = require('cors')

app.use(express.json());
app.use(cors({
    origin: 'https://deca.eco',
    methods: ['GET']
}));
app.use(helmet())

app.listen(5000, () => {

    app.get('/decaPrice', async function (req, res) {
        const dbm = await MongoClient.connect(url, {
            useUnifiedTopology: true
        }).catch(err => {
            console.log(err);
        });
        var db = dbm.db("DECAGraphics");
        db.collection("decaPrice").find({}).toArray(function (err, response) {
            if (err) throw err;
            res.json(response);
        });
    });

    app.get('/decaGeth', async function (req, res) {
        const dbm = await MongoClient.connect(url, {
            useUnifiedTopology: true
        }).catch(err => {
            console.log(err);
        });
        var db = dbm.db("DECAGraphics");
        db.collection("decaGeth").find({}).toArray(function (err, response) {
            if (err) throw err;
            res.json(response);
        });
    });

    app.get('/decaCCTS', async function (req, res) {
        const dbm = await MongoClient.connect(url, {
            useUnifiedTopology: true
        }).catch(err => {
            console.log(err);
        });
        var db = dbm.db("DECAGraphics");
        db.collection("decaCCTS").find({}).toArray(function (err, response) {
            if (err) throw err;
            res.json(response);
        });
    });

    console.log("El servidor está inicializado en el puerto 5000");
});