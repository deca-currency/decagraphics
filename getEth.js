const request = require('request-promise');

require('dotenv').config()

// Source: https://apiv2.bitcoinaverage.com/

const getEth = async(dates) => {
    const crypto = ['BTC', 'ETH', 'LTC']
    const normal = ['USD', 'GBP', 'CNY', 'EUR'];
    res = {}
    let opt = { 
        method: 'GET', 
        json: true,
        headers: {
            'X-Testing': 'testing',
            'x-ba-key': process.env.APIKEY
        },
    };
    for(element in crypto){
        opt.uri = `https://apiv2.bitcoinaverage.com/indices/global/history/${crypto[element]}USD?at=${Math.ceil(Date.now()/1000)-1}&resolution=minute`
        res[crypto[element]] = (await request(opt))['average'];
    }
    for(element in normal){
        opt.uri = `https://apiv2.bitcoinaverage.com/indices/global/history/ETH${normal[element]}?at=${Math.ceil(Date.now()/1000)-1}&resolution=minute`
        res[normal[element]] = (await request(opt))['average'];
    }
    res.dates = dates;
    return res;
};

module.exports = getEth;